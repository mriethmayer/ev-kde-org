---
title: "KDE e.V. Members"
layout: page
menu_active: Organization
---

If you are an active member of the KDE community and would like to join the KDE
e.V. please have a look at the <a href="/getinvolved/members/">information
how to become a member of the KDE e.V.</a>

These are the current active members (alphabetical by first name):

<noscript><p>This list only works if JavaScript is enabled.</p></noscript>

<ul id="memberList">
</ul>

<br style="clear:left;" />

<script>
fetch('/members-api.php')
  .then(function(response) {
    return response.json();
  })
  .then(function(members) {
    const memberList = document.getElementById('memberList');
    members.forEach(function(member) {
      const li = document.createElement('li');
      li.appendChild(document.createTextNode(member));
      memberList.appendChild(li);
    });
  });
</script>
