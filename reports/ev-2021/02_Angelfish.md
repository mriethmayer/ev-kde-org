<figure class="image-right">
    <img width="50%" src="images/Products/pp_angelfish_s.png" alt="Angelfish Browser running on a PinePhone" />
    <figcaption>The Angelfish browser works both on desktop and mobile devices.</figcaption>
</figure>

[Angelfish](https://apps.kde.org/en-gb/angelfish/) is a modern lightweight web browser that will work on your desktop and your Plasma Mobile device.

It supports typical browser features, such as

* bookmarks
* history
* tabs
