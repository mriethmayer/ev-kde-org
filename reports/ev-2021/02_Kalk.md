[Kalk ](https://apps.kde.org/kalk/)is a convergent calculator for Plasma. Kalk is a calculator application built with the [Kirigami framework](https://kde.org/products/kirigami/) which, although mainly targeted for mobile platforms, can also be used on the desktop.

Originally starting as a fork of [Liri calculator](https://github.com/lirios/calculator), Kalk has gone through heavy development, and no longer shares the same codebase with Liri calculator.

Some of its features include:

* Basic calculation
* History
* Unit conversion
* Currency conversion
* Binary calculation
