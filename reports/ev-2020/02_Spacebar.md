<figure class="image-right"> <img width="40%" src="images/Projects/spacebar.png" alt="Spacebar" /><figcaption>Spacebar.</figcaption></figure>

[Spacebar](https://apps.kde.org/spacebar/) is an app for sending and receiving SMS like chat messages. It is primarily developed for [Plasma Mobile](https://www.plasma-mobile.org/) and depends on Qt, a few KDE Frameworks (Kirigami2, KI18n, KPeople and KContacts) and telepathy-qt.

Spacebar consists of an app and a daemon. The app is user-facing and only runs while the user is reading or writing messages. The daemon runs in the background to catch incoming SMS.

The database is mostly managed by the daemon, as it is responsible for writing incoming and outgoing messages into it. It also sends notifications to the user when a new message arrived, using [KNotifications](https://api.kde.org/frameworks/knotifications/html/index.html).

The app connects directly to telepathy to also get incoming messages, to display them live in the chat. It fetches the chat history from the database. The apps also mark viewed messages as "read" in the database.

Spacebar offers users a modern-looking chat app that connects with stored contacts, providing an easy way to keep in touch and carrying out conversations over the SMS protocol.
