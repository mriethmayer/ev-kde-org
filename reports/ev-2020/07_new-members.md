KDE e.V. welcomed the following new members in 2020:

* Luiz Fernando Ranghetti
* Karl Ove Hufthammer
* Jonah Benedict Brüchert
* Iñigo Salvador Azurmendi 
* Aditya Mehra 
* Wrishiraj Kaushik 
* Méven Car
* Martin Schlander
* Johnny Jazeix
* Niccolò Venerandi 
* Johan Ouwerkerk 
* Vlad Zahorodnii 
* David Redondo
