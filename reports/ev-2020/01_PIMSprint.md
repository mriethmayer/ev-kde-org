From the 3rd to the 5th of April, members of the [KDE PIM](https://community.kde.org/KDE_PIM) team met online for the 2020 sprint.

During the sprint we discussed

* Moving [KDAV](https://api.kde.org/frameworks/kdav/html/index.html) into [KDE Frameworks](https://develop.kde.org/products/frameworks//).
* [KParts](https://api.kde.org/frameworks/kparts/html/index.html) usage in [Kontact](https://kontact.kde.org/).
* Removing the Kolab Resource and encourage users to use IMAP+DAV instead.
* Data provider plugins for the [KCalendarCore](https://api.kde.org/frameworks/kcalendarcore/html/index.html) library.
* Porting *Account Wizard* away from Kross and port it to QML, eventually replacing it completely by [KAccounts](<https://github.com/KDE/kaccounts-integration>).

What follows is an account of what we covered during the sprint and the work we carried out to improve KDE PIM throughout the subsequent weeks.

### KCalendarCore Plugin System

Nico Fella worked on this, eventually enabling platform calendar abstraction behind the [KCalendarCore](https://api.kde.org/frameworks/kcalendarcore/html/index.html) API. This means the same application code can be using a calendar from [Akonadi](https://userbase.kde.org/Akonadi) on a desktop system and the Android calendar on a phone.

We hopefully managed to sort out the remaining conceptual questions for this -- modeling hierarchies, the lazy population of expensive calendars, separate classes for the calendar metadata or not.

### Moving PIM Modules to KDE Frameworks

KDAV was nearing completion for transitioning to [Frameworks](https://develop.kde.org/products/frameworks//) after the 20.04 release, so we looked forward to solving this in May or June. A final review pass resulted in a few more improvements and API cleanups.

Following KDAV the possible candidates are the [KGAPI library](https://api.kde.org/kdepim/libkgapi/html/index.html), which is already used externally and thus would benefit most, as well as the various email frameworks (MIME, IMAP, SMTP).

### KF6/Qt6 Porting and Future-Proofing

The biggest remaining issue with the Kross usage was the account wizard. We considered three possible strategies to solve its problems:

* Replace the account wizard usage with a KAccounts-based solution.
* Port the scripted UI part to QML/JS.
* Port the scripted UI part to a widget-based plugin system.

The KAccounts-based approach would be preferable. The other two options are the fallback plan in case we don’t get that done in time for the KF6 transition.

A less severe issue is the KParts usage in Kontact, David Faure meanwhile largely completed its port to the embedded JSON based plugin loading.

### Phasing out the Kolab Resource

There’s a better maintained approach to access Kolab servers nowadays, with a combination of an IMAP and a CalDav/CardDav resources. This means dropping the dedicated Kolab resource in favor of the mentioned approach would simplify things. However, the tricky part is making a smooth transition.

### KMail

[KMail](https://apps.kde.org/kmail2/) received its usual dose of bugfixes, including:

* Fixed a crash when sending an email.
* Fixed a crash when adding too many recipients.
* Fixed a bug when KMail showed only a part of an HTML email.
* Fixed a bug when KMail did not correctly display email answer in HTML mode.
* Fixed broken message status filter.
* Fixed the maildir backend creating junk folders around and not storing the path configuration properly.
* Fixed a crash when configuring the POP3 resource.
* Fixed name of the top-level folder created by the EWS resource.
* Fixed the EWS resource not storing user password properly.

There were some exciting improvements to KMail as well: Sandro Knauß implemented support for Protected Headers for Cryptographic E-Mails. This means that we also send a signed/encrypted copy of headers and display the signed/encrypted headers (if available), and ignore the insecure headers.

Currently, we don’t obfuscate the subject to not break current workflows. Those things will be improved later on. Sandro, together with Thomas Pfeiffer, get funding from NLnet to improve mail encryption. That means more improvements were happening in the following months. The next topic they looked at was how to add Autocrypt support for KMail.

Volker Krause improved the look and feel of the “HTML Content” and “External References” warnings in emails.

As the Libre Avatar service came back from the dead a while ago, so did the support for it in KMail. The ‘Export to PDF’ feature which we introduced in the previous report was polished, and the ‘Move To Trash’ code was optimized to speed up deleting large amounts of emails.

For developers, it is now possible to open Chromium DevTools inside the message viewer pane to make it easier to debug message templates.

### KOrganizer

[KOrganizer](https://apps.kde.org/korganizer/), the calendaring and task management component has had its fair share of fixes, including:

* Fixed crashes in the category and filter managers.
* Fixed bug when a single instance of a recurring event couldn’t be changed.
* Fixed crash when creating a new TODO from Kontact.
* Fixed ‘Only resources can modify remote identifiers’ error when re-editing event.
* Fixed the DAV resource getting stuck when parse error occurs.

The Google Calendar and Google Contacts backends have been merged into a single Google Groupware resource. The change is mostly transparent to users and the old backends were migrated to the new unified backend automatically after the update. During this process, Igor Poboiko also fixed various bugs and issues in the backends and the LibKGAPI library, so a big kudos to him!

The DAV resource was changed to synchronize the calendar color from KOrganizer to the DAV server. Related to that, the menu to configure calendar color in KOrganizer was simplified by removing the “Disable Color” action.

It is now easier to recognize and set the default calendar, and the event editor now respects the settings correctly.

### KJots

[KJots](https://userbase.kde.org/KJots), the note taking application, which had been on life support for 5 years, received some love recently thanks to Igor. Most of the things are happening under the hood: some ancient dusty code was dropped, some refactoring was carried out, etc. However, if you still use KJots, you might also notice quite a number of changes too. And if you don’t use it, it’s a good time to consider using it.

Here are some of the changes:

* Fixed a data loss issue due to bugs in the Akonadi Maildir resource, which is used as a KJots backend.
* Fixed a crash on startup.
* Fixed multiple actions for the same shortcut.
* Fixed bookmarks support.
* Fixed export to plain text and HTML.
* Fixed random scrollback jumps.
* Fixed nested bullet lists breaking the undo stack.
* Link destination is displayed in the tooltip.
* Ctrl+click follows the link.
* Printing support has been revived.
* The text editing widget now supports different headings.
* Improved support for nested bullet lists.

Igor has huge plans for the future of KJots. First, more bug squashing. Second: the ability to store notes in Markdown format, synchronize with online services (thoughts are on OwnCloud/[Nextcloud](https://nextcloud.com/) or the proprietary Evernote service), and, on a lesser scale, he considered the port to the same text editing component as used by the KMail email composer. This will give KJots more text-editing features.

There are also plans to add support for inline checkboxes introduced in Qt 5.14. This would allow making checklists and TODO-lists in KJots. Another feature would be to enable sorting books and pages by their modification date, so that the most relevant would pop up first.

### Other components

Other parts of PIM also received bugfixes and improvements. [Kleopatra](https://apps.kde.org/kleopatra/), the certificate management software, now displays GPG configuration tabs and option groups always in the same order. A bug in [Akregator](https://apps.kde.org/akregator/) was fixed that could cause some feeds to have an icon missing. [KAlarm](https://apps.kde.org/kalarm/) received a bunch of UI improvements as well as some smaller features, like it is now possible to import alarms from multiple calendars at once and the calendar list is now sorted by name.

### Common Infrastructure

Lots of work went into modernizing [Akonadi](https://userbase.kde.org/Akonadi), the backend service for [Kontact](https://kontact.kde.org/). One major change was to switch to C++17 and some initial usage of C++17 features internally (public API is still C++11-compatible). Widgets for managing Tags were improved and polished and the deprecated ItemModel and CollectionModel were removed.

The [KIMAP](https://api.kde.org/kdepim/kimap/html/index.html) library was optimized to better handle large message sets. The KLDAP library can now connect to the LDAP server using SSL encryption, alongside the existing TLS support. Volker Krause worked on preparing the KDAV library (which implements the DAV protocol) into KDE Frameworks, and Laurent Montel worked through the entire PIM codebase, preparing it to port to Qt6 once it becomes available.