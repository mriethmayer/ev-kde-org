---
title: Careers in KDE
layout: page
menu_active: Organization
# Set this to false if there are no open positions; otherwise, use a list
openpositions:
  - title: Documentation Writer
    description: KDE e.V. is looking for a person with technical writer experience to update KDE‘s documentation across its various projects. Documentation for our software and community is constantly evolving and requires careful planning and maintenance, as well as regular communication and close collaboration with our contributor teams. You will be building on the outcome of previous documentation work. Please see the [call for proposals](/resources/documentationsupport-callforproposals-2021.pdf) for more details about this contracting opportunity.
  - title: Hardware Integrator
    description: Your task will range from discussing with manufacturers and evaluating the kind of hardware they are planning to ship, to making sure that it's viable for KDE to support it either by implementing the features or by coordinating with the different teams to make sure this can happen in a timely manner. This will eventually require collaborating with the system integration communities to ensure our users are getting an optimal experience. Please see the [call for proposals](/resources/callforproposals-hardware2021.pdf) for more details about this contracting opportunity.
  - title: Software Platform Engineer
    description: KDE e.V. is looking for a software engineer to develop KDE's software platform. You will be working on KDE Frameworks, Plasma, Qt, and other middleware libraries to push forward the state of the art in KDE technology. Responsibilities include developing new features, fixing bugs, performing necessary porting work, improving test coverage and reliability, and maintaining existing code. Please see the [call for proposals](/resources/callforproposals-platform2022) for more details about this contracting opportunity.
---

## Open positions

{% if page.openpositions %}
KDE e.V. is hiring! The following positions are currently open:
{% for people in page.openpositions %}
<div class="d-flex mb-4">
  <div class="mr-3">
    <img src="/corporate/pictures/open.png" width="160" height="160" />
  </div>
  <div class="people-content">
    <h3 class="mt-0">{{ people.title }}</h3>
    <p>{{ people.description | markdownify }}</p>
  </div>
</div>
{% endfor %}

Instructions for applying can be found in each position's Call for Proposals document linked above.

{% else %}
There are no open positions at the moment.
{% endif %}

You are welcome to contact kde-ev-board<span>@</span>kde.org with questions.

