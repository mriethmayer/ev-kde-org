<?php
header("Content-type: text/css");

$ldapConfig = include('ldap_config.php');
$ldapConnection = ldap_connect($ldapConfig['url'])
    or die("The LDAP URI was not parsable");

ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);

$ldapBind = ldap_bind($ldapConnection, $ldapConfig['binddn'], $ldapConfig['bindpw']);
if (!$ldapBind) {
    die('Could not bind with the LDAP server');
}

$filter = '(groupMember=ev-active)';
$result = ldap_search($ldapConnection, 'ou=people,dc=kde,dc=org', $filter, ['cn']);
$members = ldap_get_entries($ldapConnection, $result);

unset($members['count']);

usort($members, function ($a, $b) {
    $a = $a['cn'][0];
    $b = $b['cn'][0];
    $replacements = ["å"=>"a", "à"=>"a", "é"=>"e"];
    $a = strtr(mb_strtolower($a, 'UTF-8'), $replacements);
    $b = strtr(mb_strtolower($b, 'UTF-8'), $replacements);
    return strcmp($a, $b);
});

$array = [];

foreach($members as $member) {
  $array[] = $member['cn'][0];
}

echo json_encode($array);
