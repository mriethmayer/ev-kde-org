---
title: 'KDE e.V. votes 2022Q2'
date: 2022-06-20 1:30:00
layout: post
noquote: true
---

KDE e.V. makes it known that two votes took place in 2022Q2 (April-June 2022): A change to the rules of online voting, and
accepting the FLA 2.0.

- Change rules for online voting, expanding section 2.1 to allow for more phrases (email subject lines) to start a vote.
  Associated [merge request !43](https://invent.kde.org/websites/ev-kde-org/-/merge_requests/43).
- Allow the use of the Fiduciary Licensing Agreement 2.0 (for individual or entity use).
  Associated [merge request !5](https://invent.kde.org/teams/kde-ev-board/fiduciary-licensing-agreement/-/merge_requests/5).
