---
title: 'Hitflip becomes KDE e.V. Supporting Member'
date: 2007-12-21 00:00:00 
layout: post
---

The KDE e.V. is happy to welcome <a href="http://www.hitflip.de">Hitflip</a>
as a new Supporting Member. Through the continued support of KDE e.V., the Supporting
Members and Patrons make much of the KDE e.V.'s work possible and ensure well-planned
future operation to support the KDE community. The KDE e.V. thanks Hitflip and the other
Supporting Members and Patrons for their continued support.
      
