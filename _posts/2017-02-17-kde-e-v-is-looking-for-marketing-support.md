---
title: 'KDE e.V. is looking for marketing support'
date: 2017-02-17 00:00:00 
layout: post
noquote: true
---

> Applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a marketing professional to help KDE improve its marketing. Please see the <a href="https://ev.kde.org/resources/marketingsupport-callforproposals.pdf">call for proposals</a> for more details about this contract opportunity. We are looking forward to your application.
      
