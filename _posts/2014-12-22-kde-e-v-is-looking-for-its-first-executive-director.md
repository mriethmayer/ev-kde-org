---
title: 'KDE e.V. is looking for its first Executive Director'
date: 2014-12-22 00:00:00 
layout: post
noquote: true
---

> Applications for this position **are closed**.

KDE e.V. is looking for an Executive Director to take the non-profit organization to the next level. The Executive Director is responsible for managing the business of KDE e.V. and shaping its future. To find out more about the opportunity please read <a href="http://ev.kde.org/resources/executive-director-advert.pdf">the job ad</a>.
      
