---
title: 'KDE e.V. elects new board members'
date: 2022-10-26 1:30:00
layout: post
noquote: true
---

KDE e.V. held its [annual general meeting](/reports/2022-en/) at [Akademy](https://akademy.kde.org/2022). During the AGM, elections for two vacancies on the board of directors were held. Members Adriaan de Groot and Nate Graham were elected and take a seat on the [board](/corporate/board/).

We would like to thank Neofytos Kolokotronis for serving on the board during his outgoing three-year term.

There were no public votes or decisions in the third quarter of 2022 other than those described in the minutes of the AGM.
