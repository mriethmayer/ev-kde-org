---
title: 'KDE e.V. is looking for a software engineer'
layout: page
menu_active: Organization
---

KDE e.V., the non-profit organisation supporting the KDE community, is
looking to hire a software engineer to help improve the software stack that KDE software relies on.
Please see the [call for proposals]({{ '/resources/callforproposals-platform2022' | prepend: site.url }})
for more details about this contract opportunity.
We are looking forward to your application.

> The full call for proposals has more details.
