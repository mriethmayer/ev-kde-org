---
title: "KDE e.V. System Administration Working Group"
layout: page
---

### Goals

The System Administration Working Group is enabling the KDE community to work effectively and efficiently by providing and maintaining the technical infrastructure for KDE.

### Rules

> The Sysadmin Working Group has fairly stringent membership requirements
> but these are not documented here.

### Members

+ Ben Cooksley
+ Bhushan Shah
+ Kenny Coyle
+ Nicolás Alvarez

### Contact

You can reach the System Administration Working Group by mail at
[sysadmin@kde.org](mailto:sysadmin@kde.org).
